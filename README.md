# TDT2810 - Project 4, task G


#### Table of contents

  * [Rationale - why this task?](#rationale---why-this-task-)
      - [A note on `native-example-app` and project requirements](#a-note-on--native-example-app--and-project-requirements)
  * [Quick start: Project 3](#quick-start--project-3)
      - [Requirements](#requirements)
      - [Preliminary setup](#preliminary-setup)
      - [Frontend - install & run](#frontend---install---run)
      - [Backend - install & run](#backend---install---run)
      - [MongoDB setup](#mongodb-setup)
  * [Usage](#usage)
      - [Movie information](#movie-information)
      - [Search](#search)
      - [Favorites](#favorites)
  * [Testing](#testing)
  * [Troubleshooting](#troubleshooting)
      - [Node version issues](#node-version-issues)
      - [Multiple Node instances installed/checking path to Node](#multiple-node-instances-installed-checking-path-to-node)

## Rationale - why this task?

*g. Lage en tutorial realtert [sic] til prosjekt 3, med omfattende eksempler og kode på noe som er dårlig dokumentert fra før*.

In the previous project, the provided documentation was found to be disorganised and difficult to follow. The consequences were reflected in the feedback: users attempting to set up the project locally were likely to have trouble understanding, setting up and running the application. Additionally, it was remarked that the existing documentation did not adequately explain what the commands being run were doing; which may have exacerbated the problem at hand.

Project 4 aims to address this by providing a more comprehensive, step-by-step guide to setting up Project 3, and troubleshooting common issues that users may encounter.



## Quick start: Project 3

**Repository:** https://gitlab.stud.idi.ntnu.no/it2810-h22/Team-61/Prosjekt3

**Website:** http://it2810-61.idi.ntnu.no/project3/



This is an application that allows for browsing, filtering and searching for a variety of movies from a remote database (located at https://atlas-education.s3.amazonaws.com/sampledata.archive); and consists of a React-based frontend and Express backend, with MongoDB being used for the database.



#### Requirements

Before continuing, please ensure that [Git](https://git-scm.com/) and [Node.js](https://nodejs.org/en/download/) are installed on your machine (setup procedure may vary depending on your OS, and is outside the scope of this tutorial).

Additionally, for the backend & database to work correctly, [MongoDB](https://www.mongodb.com/docs/manual/installation/) and [MongoDB Database Tools](https://www.mongodb.com/try/download/shell) are required to be installed.



#### Preliminary setup

First, clone and enter this repository:

```
git clone https://gitlab.stud.idi.ntnu.no/richarah/Prosjekt4.git
cd Prosjekt4
```



#### Frontend - install & run

Enter the `my-app` directory containing the frontend:

```
cd my-app
```

Next, install the necessary dependencies and start the frontend as follows. `npm start` will automatically handle the frontend startup via `react-scripts`, so no further attention is necessary:

```
npm i && npm start
```



#### Backend - install & run

**Please note:** Since the backend uses a separate `package.json` from the frontend to ease maintainability & serviceability, `npm i` (shorthand for `npm install`) has to be run separately from the backend folder.



First, switch the directory containing the backend:

```
cd backend
```

From within this folder, execute `npm i` to install the required dependencies. Lastly, run `node server.js` to start the server:

```
npm i && node server.js
```



#### MongoDB setup

**Please note:** This tutorial assumes the use of a machine running Linux. For setup instructions on Windows or other operating systems, please refer to the [official guide included in the MongoDB documentation](https://www.mongodb.com/docs/database-tools/installation/installation-windows/).



Start the MongoDB daemon process and specify a configuration file path:

```
mongod --config /usr/local/etc/mongod.conf
```

Download the remote database to a local file (using the `-o` option to select a different name if desired)

```
curl https://atlas-education.s3.amazonaws.com/sampledata.archive -o sampledata.archive
```

Lastly, start the database from the local archive:
```
mongorestore --archive=sampledata.archive --db=sample_mflix --collection=movies
```



## Usage

Once the above installation & setup steps have been fulfilled, the main page should appear as follows:

![mainpage](./my-app/public/readme_pic/mainpage.PNG)



#### Movie information

Clicking on a movie will display further details & information about it - see below:

![movie](./my-app/public/readme_pic/movie.PNG)



#### Search

Movie titles may be searched and filtered through using the provided search bar:

![movie](./my-app/public/readme_pic/search.PNG)

As only a certain number of movies may be displayed at once, the site will use multiple pages of movies if the search results are numerous enough. These may be cycled using the `Prev` and `Next` buttons at the bottom of the page:

![movie](./my-app/public/readme_pic/pages.PNG)

#### Favorites

Movies may be added/removed to and from a locally-stored list of favorites with the `Add to favourites`/`Remove from favourites` button corresponding to a given movie. This list may then be accessed with the `My favourites` menu in the upper left.

![movie](./my-app/public/readme_pic/favorite.PNG)



## Testing

Jest and Puppeteer have been used to achieve end-to-end testing, allowing the tester to mimic the interactions of inputting "Horror" in the search field, then clicking the first movie in the results. To check if we have the expected result, we evaluate whether the title is "100 Bloody Acres" as expected.

The test harness may be executed as follows:

```
cd my-app && npm test
```



## Troubleshooting



#### Node version issues

In certain cases, NodeJS/ failing to detect or install a given package may be due to a deprecated version of Node being installed. To check this, run the following:

```
node --version
```

At the time of writing, Node release `v16.18.1` appears to work stably with this application. To install this version from the `snap` store used by Ubuntu (Linux distro used on provided VM), execute the following:

```
sudo snap install node --classic --channel=16
```

Running `node --version` should now return `v16.xx.xx` with the crosses replaced with the most recent stable patch of `v16`.



#### Multiple Node instances installed/checking path to Node

If the system has multiple versions of Node installed, the version may not appear to change in spite of the above update, this may be due to another instance in the way.

To check where `node` is being executed from on a Linux system, run the following:

```
which node
```

If using an installation from `snap`, the above should return `/snap/bin/node`.
